package br.edu.unisep.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityParentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityParentApplication.class, args);
	}

}
